package ru.teterin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.ITaskEndpoint;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.dto.Task;
import ru.teterin.tm.entity.TaskPOJO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "ru.teterin.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.taskService = serviceLocator.getTaskService();
    }

    @Override
    @WebMethod
    public void mergeTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "task") @Nullable final Task task
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        task.setUserId(userId);
        taskService.merge(userId, task.toTaskPOJO(serviceLocator));
    }

    @Override
    @WebMethod
    public void persistTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "task") @Nullable final Task task
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        task.setUserId(userId);
        taskService.persist(userId, task.toTaskPOJO(serviceLocator));
    }

    @NotNull
    @Override
    @WebMethod
    public Task findOneTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final Task task = taskService.findOne(userId, taskId).toTask();
        return task;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTask(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final List<TaskPOJO> tasks = taskService.findAll(userId);
        @NotNull final List<Task> result = new ArrayList<Task>();
        for (@NotNull final TaskPOJO taskPOJO : tasks) {
            result.add(taskPOJO.toTask());
        }
        return result;
    }

    @Override
    @WebMethod
    public void removeTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        taskService.remove(userId, taskId);
    }

    @Override
    @WebMethod
    public void removeAllTasks(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        taskService.removeAll(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> searchTaskByString(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final List<TaskPOJO> tasks = taskService.searchByString(userId, string);
        @NotNull final List<Task> result = new ArrayList<Task>();
        for (@NotNull final TaskPOJO taskPOJO : tasks) {
            result.add(taskPOJO.toTask());
        }
        return result;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> sortAllTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "sortOption") @Nullable final String sortOption
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final List<TaskPOJO> tasks = taskService.findAndSortAll(userId, sortOption);
        @NotNull final List<Task> result = new ArrayList<Task>();
        for (@NotNull final TaskPOJO taskPOJO : tasks) {
            result.add(taskPOJO.toTask());
        }
        return result;
    }

    @Override
    @WebMethod
    public void linkTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "projectId") @Nullable final String projectId,
        @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        taskService.linkTask(userId, projectId, taskId);
    }

}
