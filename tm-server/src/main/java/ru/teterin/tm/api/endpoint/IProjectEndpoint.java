package ru.teterin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.dto.Project;
import ru.teterin.tm.dto.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    public void mergeProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "project") @Nullable final Project project
    ) throws Exception;

    @WebMethod
    public void persistProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "project") @Nullable final Project project
    ) throws Exception;

    @NotNull
    @WebMethod
    public Project findOneProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception;

    @NotNull
    @WebMethod
    public List<Project> findAllProject(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception;

    @WebMethod
    public void removeProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception;

    @WebMethod
    public void removeAllProjects(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception;

    @NotNull
    @WebMethod
    public List<Project> searchProjectByString(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "string") @Nullable final String string
    ) throws Exception;

    @NotNull
    @WebMethod
    public List<Project> sortAllProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "sortOption") @Nullable final String sortOption
    ) throws Exception;

    @NotNull
    @WebMethod
    public List<Task> findAllProjectTasks(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception;

}
