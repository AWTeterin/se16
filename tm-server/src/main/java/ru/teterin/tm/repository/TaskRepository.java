package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.TaskPOJO;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    @NotNull
    private final EntityManager entityManager;

    public TaskRepository(
        @NotNull final EntityManager entityManager
    ) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public TaskPOJO findOne(
        @NotNull final String userId,
        @NotNull final String id
    ) {
        @NotNull final List<TaskPOJO> tasks = entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.id = :id AND t.user.id = :userId", TaskPOJO.class)
            .setParameter("id", id).setParameter("userId", userId).getResultList();
        if (tasks.isEmpty()) {
            return null;
        }
        return tasks.get(0);
    }

    @NotNull
    @Override
    public List<TaskPOJO> findAll() {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t", TaskPOJO.class).getResultList();
    }

    @NotNull
    @Override
    public List<TaskPOJO> findAll(
        @NotNull final String userId
    ) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.user.id = :userId", TaskPOJO.class)
            .setParameter("userId", userId).getResultList();
    }

    @Override
    public void persist(
        @NotNull final TaskPOJO task
    ) {
        entityManager.persist(task);
    }

    @Override
    public void merge(
        @NotNull final TaskPOJO task
    ) {
        entityManager.merge(task);
    }

    @Override
    public void remove(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception {
        @Nullable final TaskPOJO task = findOne(userId, id);
        if (task == null) {
            entityManager.close();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        }
        entityManager.remove(task);
    }

    @Override
    public void removeAll() {
        @NotNull final List<TaskPOJO> tasks = findAll();
        for (@NotNull final TaskPOJO task : tasks) {
            entityManager.remove(task);
        }
    }

    @Override
    public void removeAll(
        @NotNull final String userId
    ) {
        @NotNull final List<TaskPOJO> tasks = findAll(userId);
        for (@NotNull final TaskPOJO task : tasks) {
            entityManager.remove(task);
        }
    }

    @NotNull
    @Override
    public List<TaskPOJO> searchByString(
        @NotNull final String userId,
        @NotNull final String searchString
    ) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.user.id = :userId " +
            "AND (t.name LIKE CONCAT('%', :searchString,'%') OR t.description LIKE CONCAT('%', :searchString,'%'))", TaskPOJO.class)
            .setParameter("userId", userId).setParameter("searchString", searchString).getResultList();
    }

    @NotNull
    @Override
    public List<TaskPOJO> sortByDateCreate(
        @NotNull final String userId,
        @NotNull final String sortType
    ) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.user.id = :userId ORDER BY dateCreate", TaskPOJO.class)
            .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<TaskPOJO> sortByDateStart(
        @NotNull final String userId,
        @NotNull final String sortType
    ) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.user.id = :userId ORDER BY dateStart", TaskPOJO.class)
            .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<TaskPOJO> sortByDateEnd(
        @NotNull final String userId,
        @NotNull final String sortType
    ) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.user.id = :userId ORDER BY dateEnd", TaskPOJO.class)
            .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<TaskPOJO> sortByStatus(
        @NotNull final String userId,
        @NotNull final String sortType
    ) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.user.id = :userId ORDER BY status", TaskPOJO.class)
            .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<TaskPOJO> findAllByProjectId(
        @NotNull final String userId,
        @NotNull final String projectId
    ) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t " +
            "WHERE t.user.id = :userId AND t.project.id = :projectId", TaskPOJO.class)
            .setParameter("userId", userId).setParameter("projectId", projectId)
            .getResultList();
    }

}
