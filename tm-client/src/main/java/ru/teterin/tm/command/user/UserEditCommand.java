package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Exception_Exception;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class UserEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit user login.";
    }

    @Override
    public void execute() throws Exception_Exception {
        terminalService.print(Constant.USER_EDIT);
        @Nullable final String session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        terminalService.print(Constant.ENTER_NEW_LOGIN);
        @Nullable final String newLogin = terminalService.readString();
        userEndpoint = serviceLocator.getUserEndpoint();
        final boolean loginIsBusy = !userEndpoint.loginIsFree(newLogin);
        if (loginIsBusy) {
            terminalService.print(Constant.LOGIN_BUSY);
            return;
        }
        userEndpoint.setLogin(session, newLogin);
        stateService.setLogin(newLogin);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
