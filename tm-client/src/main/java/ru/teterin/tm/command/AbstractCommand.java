package ru.teterin.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.*;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.ITerminalService;

import java.lang.Exception;

public abstract class AbstractCommand {

    @Setter
    @NotNull
    protected ITerminalService terminalService;

    @Setter
    @NotNull
    protected IServiceLocator serviceLocator;

    @Setter
    @NotNull
    protected IStateService stateService;

    @NotNull
    protected IUserEndpoint userEndpoint;

    @NotNull
    protected IProjectEndpoint projectEndpoint;

    @NotNull
    protected ITaskEndpoint taskEndpoint;

    @NotNull
    protected ISessionEndpoint sessionEndpoint;

    @NotNull
    protected IDomainEndpoint domainEndpoint;

    public AbstractCommand() {
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract boolean secure();

    @NotNull
    public Role[] roles() {
        @NotNull final Role[] all = new Role[]{Role.USER, Role.ADMIN};
        return all;
    }

}
