package ru.teterin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Exception_Exception;
import ru.teterin.tm.api.endpoint.Task;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task for project.";
    }

    @Override
    public void execute() throws Exception_Exception {
        terminalService.print(Constant.TASK_CREATE);
        @Nullable final String session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        terminalService.print(Constant.ENTER_PROJECT_ID);
        @Nullable final String projectId = terminalService.readString();
        projectEndpoint = serviceLocator.getProjectEndpoint();
        projectEndpoint.findOneProject(session, projectId);
        @NotNull final Task task = terminalService.readTask();
        task.setProjectId(projectId);
        taskEndpoint = serviceLocator.getTaskEndpoint();
        taskEndpoint.persistTask(session, task);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
