package ru.teterin.tm;

import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public final class SessionEndpointTest extends AbstractTest {

    @Test
    public void openSession() throws Exception {
        userEndpoint.persistUser(user);
        @Nullable final String userToken = sessionEndpoint.openSession(user.getLogin(), user.getPassword());
        Assert.assertNotNull(userToken);
        userEndpoint.removeUser(userToken, userId);
    }

    @Test
    public void closeSession() throws Exception {
        userEndpoint.persistUser(user);
        @Nullable final String userToken = sessionEndpoint.openSession(user.getLogin(), user.getPassword());
        Assert.assertNotNull(userToken);
        sessionEndpoint.closeSession(userToken);
        try {
            userEndpoint.removeUser(userToken, userId);
            Assert.fail("Excepted AcessForbiddenException");
        } catch (Exception ignored) {
        }
        userEndpoint.removeUser(adminToken, userId);
    }

}
